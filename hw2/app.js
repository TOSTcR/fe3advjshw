const books = [
	{
		author: 'Люсі Фолі',
		name: 'Список запрошених',
		price: 70,
	},

	{
		author: 'Сюзанна Кларк',
		name: 'Джонатан Стрейндж і м-р Норрелл',
	},
	{
		name: 'Дизайн. Книга для недизайнерів.',
		price: 70,
	},
	{
		author: 'Алан Мур',
		name: 'Неономікон',
		price: 70,
	},
	{
		author: 'Анґус Гайленд',
		name: 'Коти в мистецтві',
	},
	{
		author: 'Террі Пратчетт',
		name: 'Рухомі картинки',
		price: 40,
	},
]

class BookInfo {
	constructor(...books) {
		this.books = books
	}

	render(root) {
		try {
			this.books.forEach(el => {
				const list = document.createElement('ul')

				el.forEach(item => {
					const listItem = document.createElement('li')
					if (
						item.name !== undefined &&
						item.author !== undefined &&
						item.price !== undefined
					) {
						listItem.innerText = `${item.name} ${item.author} ${item.price}`
						list.appendChild(listItem)
						root.appendChild(list)
						return listItem
					}
					if (
						item.name === undefined ||
						item.author === undefined ||
						item.price === undefined
					) {
						listItem.remove()
						setTimeout(() => {
							throw Error('Object don`t have important option')
						}, 1000)
					}
				})
			})
		} catch (e) {
			console.error(e.message)
		}
	}
}
const root = document.getElementById('root')
const bookInfo = new BookInfo(books)
bookInfo.render(root)
// console.log(bookInfo)

class Employee {
	constructor(name, age, salary) {
		this.name = name
		this.age = age
		this.salary = salary
	}
	get Name() {
		return `${this.name}`
	}
	set Name(value) {
		this.name = value
	}

	get Age() {
		return `${this.age}`
	}
	set Age(value) {
		this.age = value
	}

	get Salary() {
		return `${this.salary}`
	}
	set Salary(value) {
		this.salary = value
	}
}

class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary)
		this.lang = lang
	}

	get Salary() {
		return `${this.salary}` * 3
	}
	set Salary(value) {
		this.salary = value * 3
	}
}

const employee = new Employee('Andrey', 21, 21000)
const programmer1 = new Programmer('Yuriy', 33, 36000)
const programmer2 = new Programmer('Anton', 18, 20000)
console.log(programmer1)
console.log(programmer2)
